<?php
	require_once __DIR__ . "/vendor/autoload.php";
	echo extension_loaded("mongodb") ? "loaded\n" : "not loaded\n";
	// connect to mongodb
	$m = new MongoDB\Driver\Manager("mongodb://localhost:27017");

	$collection = (new MongoDB\Client)->test->users;

	$insertOneResult = $collection->insertOne([
	    'username' => 'admin',
	    'email' => 'admin@example.com',
	    'name' => 'Admin User',
	]);

	printf("Inserted %d document(s)\n", $insertOneResult->getInsertedCount());

	var_dump($insertOneResult->getInsertedId());	

	
	$cursor = $collection->find(['email' => 'admin@example.com']);

	foreach ($cursor as $document) {
	    echo "<pre>";
	    print_r($document);
	}
?>